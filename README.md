# Maps App

Maps app is built in Angular where you navigate the world.

## Mapbox API

Mapbox API is used to fetch data in this app.

`https://www.mapbox.com`

## Live Demo

[Check here](https://alpertoy.gitlab.io/maps-app/)
