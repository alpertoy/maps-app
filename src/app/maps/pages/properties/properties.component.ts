import { Component, OnInit } from '@angular/core';

interface Property {
  title: string;
  description: string;
  lngLat: [number, number];
}
@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styles: [
  ]
})
export class PropertiesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  properties: Property[] = [
    {
      title: 'Miami Beach, USA',
      description: 'Beautiful and famous beach in Miami, USA',
      lngLat: [-80.12818732617757, 25.783855592396655]
    },
    {
      title: 'The Cosmopolitan Hotel, USA',
      description: 'Amazing hotel in Las Vegas, USA',
      lngLat: [-115.17343702009124, 36.10988529570512]
    },
    {
      title: 'Le Fouquets, France',
      description: 'One of the top food destinations in Paris, France',
      lngLat: [2.301423859435014, 48.871465148305575]
    },
    {
      title: 'Northern Lights Village, Finland',
      description: 'Northern Lights Village in Saariselka, Finland',
      lngLat: [27.4032276983065, 68.42408324756519]
    }
  ]

}
